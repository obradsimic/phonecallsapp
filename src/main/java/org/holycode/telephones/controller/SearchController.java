package org.holycode.telephones.controller;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.holycode.telephones.model.CallDto;
import org.holycode.telephones.model.CallType;
import org.holycode.telephones.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchController {
	
	private final SearchService service;
	
	final static Logger logger = Logger.getLogger(SearchController.class);
	
	@Autowired
	public SearchController(SearchService service) {
		this.service = service;
	}
	
	@RequestMapping("/getCalls")
	public Collection<CallDto> search(@RequestParam(value = "phone", required = false) String phone,
									 @RequestParam(value = "dateFrom", required = false) String dateFrom,
									 @RequestParam(value = "dateTo", required = false) String dateTo,
									 @RequestParam(value = "callType", required = false) CallType callType) {
		
		try {
			if (StringUtils.isBlank(phone)) 
				throw new IllegalArgumentException("Phone number can't be blank!");
			
			return service.getCalls(phone, dateFrom, dateTo, callType);
		} catch (Exception ex) {
			logger.error(ex);
		}
		
		return null;
	}
}
