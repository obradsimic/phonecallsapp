package org.holycode.telephones.config;

import org.neo4j.ogm.session.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

@Configuration
@ComponentScan(basePackages = { "org.holycode.telephones" })
@EnableNeo4jRepositories(basePackages = "org.holycode.telephones")
public class DatabaseNeo4jConfig {
	
	@Value("${db.credentials.username}") 
	private String username;
	
	@Value("${db.credentials.password}")
	private String password;
	
	@Value("${db.uri}")
	private String uri;
	
	@Value("${db.driver}")
	private String driver;
	
	@Bean
    public org.neo4j.ogm.config.Configuration getConfiguration() {
		
        org.neo4j.ogm.config.Configuration config = new org.neo4j.ogm.config.Configuration();
        
        config.driverConfiguration()
        	.setDriverClassName(driver)
        	.setCredentials(username, password)
        	.setURI(uri);
        
        return config;
    }

    @Bean
    public SessionFactory getSessionFactory() {
        return new SessionFactory(getConfiguration(), "org.holycode.telephones");
    }
    
    @Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
