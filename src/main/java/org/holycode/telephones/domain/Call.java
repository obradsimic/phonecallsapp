package org.holycode.telephones.domain;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = "Called")
public class Call {
	
	@GraphId
	private Long id;
	
	private Long date;
	
	@StartNode
	private Phone caller;

	@EndNode
	private Phone receiver;

	public Call() {
		
	}

	public Call(Phone caller, Phone receiver, Long date) {
		this.caller = caller;
		this.receiver = receiver;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public Phone getCaller() {
		return caller;
	}

	public void setCaller(Phone caller) {
		this.caller = caller;
	}

	public Phone getReceiver() {
		return receiver;
	}

	public void setReceiver(Phone receiver) {
		this.receiver = receiver;
	}
}
