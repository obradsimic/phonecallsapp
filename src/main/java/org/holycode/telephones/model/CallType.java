package org.holycode.telephones.model;

public enum CallType {
	INCOMING,
	OUTGOING,
	ALL
}
