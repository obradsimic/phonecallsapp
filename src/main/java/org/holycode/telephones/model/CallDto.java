package org.holycode.telephones.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class CallDto {
	private Long caller;
	private Long receiver;
	
	private Date date;

	public CallDto(Long caller, Long receiver, Date date) {
		this.caller = caller;
		this.receiver = receiver;
		this.date = date;
	}

	public Long getCaller() {
		return caller;
	}

	public void setCaller(Long caller) {
		this.caller = caller;
	}

	public Long getReceiver() {
		return receiver;
	}

	public void setReceiver(Long receiver) {
		this.receiver = receiver;
	}
	
	@JsonFormat(pattern = "MM-dd-yyyy HH:mm:ss")
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
