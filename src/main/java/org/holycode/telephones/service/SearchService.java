package org.holycode.telephones.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.holycode.telephones.domain.Call;
import org.holycode.telephones.model.CallDto;
import org.holycode.telephones.model.CallType;
import org.holycode.telephones.repository.CallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SearchService {
	
	private final CallRepository repo;
	private SimpleDateFormat dateFormat;
	
	@Autowired
	public SearchService(CallRepository repo,
								   @Value("${db.file.path}") String filePath) {
		this.repo = repo;
		
		dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		repo.clearCalls();
		repo.clearPhones();
		
		repo.init(filePath);
	}
	
	private List<CallDto> toDto(Collection<Call> calls) {
		List<CallDto> result = new ArrayList<>();

		Iterator<Call> iterator = calls.iterator();
		
		while (iterator.hasNext()) {
			Call tmp = iterator.next();
			
			result.add( new CallDto(tmp.getCaller().getNumber(),
								    tmp.getReceiver().getNumber(), 
								    new Date(tmp.getDate() * 1000)) );
		}
		
		
		Collections.sort(result, new Comparator<CallDto>() {

			@Override
			public int compare(CallDto c1, CallDto c2) {
				return c1.getDate().after(c2.getDate()) ? 1 : -1;
			}
			
		});
		
		
		return result;
	}
	
	private Long toTimestamp(String date) throws Exception {
	    Date parsedTimeStamp = dateFormat.parse(date);
	    
	    return (new Timestamp(parsedTimeStamp.getTime()).getTime())/1000L;
	}
	
	public List<CallDto> getAllIncomingCalls(Long number, Long dateFromTimeStamp, Long dateToTimeStamp){
		return toDto(repo.getIncomingCalls(number, dateFromTimeStamp, dateToTimeStamp));
	}
	
	public List<CallDto> getAllOutgoingCalls(Long number, Long dateFromTimeStamp, Long dateToTimeStamp){
		return toDto(repo.getOutgoingCalls(number, dateFromTimeStamp, dateToTimeStamp));
	}
	
	public List<CallDto> getAllCalls(Long number, Long dateFromTimeStamp, Long dateToTimeStamp){
		return toDto(repo.getAllCalls(number, dateFromTimeStamp, dateToTimeStamp));
	}

	public List<CallDto> getCalls(String phone, String dateFrom, String dateTo, CallType callType) throws Exception {
		
		Long dateFromTimeStamp = StringUtils.isBlank(dateFrom) ? 0L : toTimestamp(dateFrom);
		Long dateToTimeStamp = StringUtils.isBlank(dateTo) ? ((new Date()).getTime())/1000L : toTimestamp(dateTo);
		
		if (callType.equals(CallType.INCOMING)) {
			return getAllIncomingCalls(Long.parseLong(phone), dateFromTimeStamp, dateToTimeStamp);
		} else if (callType.equals(CallType.OUTGOING)) {
			return getAllOutgoingCalls(Long.parseLong(phone), dateFromTimeStamp, dateToTimeStamp);
		} else if (callType.equals(CallType.ALL)){
			return getAllCalls(Long.parseLong(phone), dateFromTimeStamp, dateToTimeStamp);
		}
		
		return null;
	}
}
