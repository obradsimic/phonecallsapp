package org.holycode.telephones.repository;

import java.util.Collection;

import org.holycode.telephones.domain.Phone;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

public interface PhoneRepository extends GraphRepository<Phone> {
	Phone findByNumber(@Param("number") Long number);
	
	@Query("match (caller:Phone)-[r:Called]->(receiver:Phone) "
		 + "where caller.number = {number} "
		 + "and r.date=1469872632 "
		 + "return caller,r,receiver")
	Collection<Phone> getNumbersCalled(@Param("number") Long number);
	
	@Query("MATCH (n:Phone) "
			 + "RETURN n "
			 + "LIMIT 20")
	Collection<Phone> getAll();
}
