package org.holycode.telephones.repository;

import java.util.Collection;

import org.holycode.telephones.domain.Call;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

public interface CallRepository extends GraphRepository<Call>  {
	
	@Query("match p=(caller)-[call:Called]->() "+ 
		   "where caller.number = {number} " + 
		   "and call.date > {dateFrom} and call.date < {dateTo} " +
		   "return p")
	Collection<Call> getOutgoingCalls(@Param("number") Long number,
									  @Param("dateFrom") Long dateFrom,
									  @Param("dateTo") Long dateTo);
	
	@Query("match p=()-[call:Called]->(receiver) "+ 
			   "where receiver.number = {number} " +
			   "and call.date > {dateFrom} and call.date < {dateTo} " +
			   "return p")
	Collection<Call> getIncomingCalls(@Param("number") Long number,
									  @Param("dateFrom") Long dateFrom,
									  @Param("dateTo") Long dateTo);
	
	@Query("match p=(caller)-[call:Called]->(receiver) "+ 
			   "where (receiver.number = {number} or caller.number = {number}) " +
			   "and call.date > {dateFrom} and call.date < {dateTo} " +
			   "return p")
	Collection<Call> getAllCalls(@Param("number") Long number,
			                     @Param("dateFrom") Long dateFrom,
			 				     @Param("dateTo") Long dateTo);

	@Query("start r=relationship(*) delete r")
	void clearCalls();
	
	@Query("match (p:Phone) delete p")
	void clearPhones();
	
	@Query("load csv with headers " 
         + "from {filePath} "
         + "as csvLine "
         + "fieldterminator \';\' "
         + "create(caller:Phone{number: toInt(csvLine['A_NUMBER'])}) "
         + "create(receiver:Phone{number: toInt(csvLine['B_NUMBER'])}) "
         + "create (caller)-[:Called{date: apoc.date.parse(csvLine['EVENT_DATE'],\"s\",\"dd.MM.yyyy HH:mm\")}]->(receiver)")
	void init(@Param("filePath") String filePath);
}
