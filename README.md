# README #

### What is this repository for? ###

This repository contains source code of HolyCode's
Practical Assingment for Java Developers (Part 1).

This application imports data from CSV file to Neo4j base.
Data consists of phone numbers (Nodes) and calls (Links).

Applicaiton offers search for incoming, outgoing and all calls
for given phone number and date range. 

### Prerequisites ###

In order to successfully start this application,
Neo4j server with empty database should be started.
Also, I used APOC plugin to work with dates, so it needs to be installed.
Because I received .numbers file and converted it to .csv (https://cloudconvert.com/numbers-to-csv)
EVENT_DATE format is expected to be like this: 
```javascript
dd.MM.yyyy HH:mm
```
Proper date is, for example: 
```javascript
30.07.2016 9:57
```

### Neo4j Configuration ###

Configuration file is **conf/neo4j.conf** and all configurations
provided bellow are related to that file.I used **org.neo4j.ogm.drivers.http.driver.HttpDriver** to connect to database from Java,
so following db configurations must be enabled:
```
# HTTP Connector. There must be exactly one HTTP connector.
dbms.connector.http.enabled=true
dbms.connector.http.listen_address=:7474

# HTTPS Connector. There can be zero or one HTTPS connectors.
dbms.connector.https.enabled=true
dbms.connector.https.listen_address=:7473
```

I created empty database using Neo4j Desktop application which
by default specifies that files can be imported only from **import** directory:
```
# This setting constrains all `LOAD CSV` import files to be under the `import` directory. Remove or comment it out to
# allow files to be loaded from anywhere in the filesystem; this introduces possible security problems. See the
# `LOAD CSV` section of the manual for details.
dbms.directories.import=import
```

### Technologies ###

Technologies that I used to implement this solution are:
```
1.   Neo4j
2.   Spring Boot framework
```

### Setup ###

This application contains application.properties file.
Purpose of that file is to make application configurable from outside, without changing source code.
There are five configurable properties located in application.properties file:
```
db.credentials.username: Username of the Neo4j db owner
db.credentials.password: Password of the Neo4j db owner
db.uri: URI of the database
db.driver: Driver that is used
db.file.path: Path to the file to be imported
```

Proper application.properties file (for my local envirnoment) looks like this:
```javascript
db.credentials.username=neo4j
db.credentials.password=admin
db.uri=http://localhost:7474
db.driver=org.neo4j.ogm.drivers.http.driver.HttpDriver
db.file.path=file:///numbers.csv
```
	